package com.Diplomski.WrapperService.Handlers.Exceptions;

import com.Diplomski.WrapperService.Handlers.Exceptions.base.HandledException;
import org.springframework.http.HttpStatus;

public class NoMainMethodFoundException extends HandledException {

    private static final String errorMessage = "No main methode found in user code.";

    public NoMainMethodFoundException() {
        super(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
